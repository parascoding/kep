import java.util.*;
import java.io.*;
public class Main{
    static BufferedReader br;
    static PrintWriter ot;
    public static void main(String args[]) throws IOException{
        try{
            br = new BufferedReader(new FileReader("input.txt"));
            ot = new PrintWriter(new FileWriter("output.txt"));

            list = new ArrayList<>();

            takeInput();
            
            findUniqueItemsAndTheirOccurence();
            
            arrangeItemsBasedOnSupport();

            constructFPTree();

            printFPTree();
            ot.println(map+"\n"+list);
            System.out.print("Came at 22");
            buildConditionalPatternBase();
            ot.println("Conditional Pattern Base -");
            printCPB();

            buildCFPT();
            ot.println("\nConditional Frequent Pattern Tree -");
            ot.println(mapCFPT);
            ot.close();
        } catch(Exception e){
            e.printStackTrace();
            return;
        }
    }
    static void takeInput(){
        try{
            String S;
            while((S=br.readLine())!=null){
                String s[] = S.split(" ");
                List<Integer> temp = new ArrayList<>();
                for(int j=0;j<s.length;j++){
                    temp.add(Integer.parseInt(s[j]));
                }
                list.add(new ArrayList<>(temp));
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    static void findUniqueItemsAndTheirOccurence(){
        map = new HashMap<>();
        for (List<Integer> x : list){
            for(int y : x){
                map.put(y, map.getOrDefault(y, 0) + 1);
            }
        }
    }
    static void arrangeItemsBasedOnSupport(){
        for(int i = 0; i < list.size(); i++){
            List<Integer> x = list.get(i);
            Collections.sort(x, 
                new Comparator<Integer>(){
                    public int compare(Integer x1, Integer x2){
                        if (map.get(x1) == map.get(x2))
                            return x1 - x2;
                        return map.get(x2) - map.get(x1);
                    }
                }
            );

            List<Integer> tempList = new ArrayList<>();
            for(int y:x){
                if(map.get(y) < 3){
                    break;
                }
                tempList.add(y);
            }

            list.set(i, new ArrayList<>(tempList));
        }
    }
    static void printBranch(Trie node){
        if(node==null) return;

        ot.print("["+node.itemId+" "+node.freq+"] ");

        for(Map.Entry<Integer, Trie> e : node.child.entrySet()){
            printBranch(e.getValue());
        }
    }
    static void printFPTree(){
        for(Map.Entry<Integer, Trie> e : root.child.entrySet()){
            printBranch(e.getValue());
            ot.println();
        }
    }
    static void insert(List<Integer> trscn){
        Trie node = root;

        for(int x : trscn){
            if (!node.child.containsKey(x)){
                node.child.put(x, new Trie());
            }
            node.child.get(x).itemId = x;
            node.child.get(x).freq++;     
            node.child.get(x).parent = node;
            node = node.child.get(x);
        }
    }
    static void constructFPTree(){
        root = new Trie();

        for(List<Integer> x : list){
            insert(x);
        }

    }
    static void findLeavesUtil(Trie node, List<Trie> leaves){
        if(node.flag) return;
        if( node.isLeaf()){
            leaves.add(node);
            return;
        }
        for(Map.Entry<Integer, Trie> e : node.child.entrySet()){
            findLeavesUtil(e.getValue(), leaves);
        }
    }
    static List<Trie> findLeaves(){
        List<Trie> leaves = new ArrayList<>();
        findLeavesUtil(root, leaves);
        return leaves;
    }
    
    static List<Trie> getReversedPath(Trie node){
        node = node.parent;
        List<Trie> ret = new ArrayList<>();
        while(node!=null){
            ret.add(node);
            node = node.parent;
        }
        return ret;
    }
    static void insert(Trie node, List<Trie> path, int freq){
        for(Trie trieNode : path){
            if(!node.child.containsKey(trieNode.itemId)){
                node.child.put(trieNode.itemId, new Trie());
            }
            node.child.get(trieNode.itemId).itemId = trieNode.itemId;
            node.child.get(trieNode.itemId).freq += freq;
            node = node.child.get(trieNode.itemId);
        }
    }
    static void buildConditionalPatternBase(){
        List<Trie> leaves;

        mapCPB=new HashMap<>();
        while((leaves = findLeaves()).size()>0){
            ot.println("Leaves: "+leaves);  
            for(Trie x : leaves){
                List<Trie> path = getReversedPath(x);
                if(!mapCPB.containsKey(x.itemId)){
                    mapCPB.put(x.itemId, new Trie());
                }
                if(path.size()>0){
                    path.remove(path.size()-1);
                    Collections.reverse(path);
                    insert(mapCPB.get(x.itemId), path, x.freq);
                }
                ot.println("Path: "+path);
                ot.flush();
                x.flag = true;
                if(x.parent!=null){
                    System.out.println(x+" "+x.parent);
                    x.parent.numOfChildDeleted++;   
                }
            }
        }

    }
    static void buildCFPT(){
        mapCFPT = new HashMap<>();
        for(Map.Entry<Integer, Trie> e:mapCPB.entrySet()){
            mapCFPT.put(e.getKey(), new ArrayList<>());
            fillCFPT(e.getValue(), mapCFPT.get(e.getKey()));
        }
    }
    static void fillCFPT(Trie node, List<Trie> list){
        if(node.freq >= 3){
            list.add(node);
        }
        for( Map.Entry<Integer, Trie> e : node.child.entrySet()){
            fillCFPT(e.getValue(), list);
        }
    }
    static void printCPB(){
        for(Map.Entry<Integer, Trie> e:mapCPB.entrySet()){
            ot.println(e.getKey());
            printBranch(e.getValue());
            ot.println();
        }
    }
    static Map<Integer, List<Trie>> mapCFPT;
    static Map<Integer, Trie> mapCPB;
    static Trie root;
    static Map<Integer, Integer> map;
    static List<List<Integer>> list;
    static class Trie{
        int itemId, freq;
        int numOfChildDeleted;
        boolean flag;
        Map<Integer, Trie> child;
        Trie parent;
        Trie(){
            freq = 0;
            numOfChildDeleted = 0;
            child = new HashMap<>();

        }
        boolean isLeaf(){
            return numOfChildDeleted == child.size();
        }
        @Override
        public String toString(){
            return "["+itemId+","+ freq+" ]";
        }
    }
}