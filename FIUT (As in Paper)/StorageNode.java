package fiut;
class StorageNode{
    Map<Character, StorageNode> child;
    char itemId;
    int freq;
    boolean isEnd;
    StorageNode(char itemId){
        this.itemId = itemId;
        freq = 1;
        isEnd = false;
        child = new HashMap<>();
    }
}