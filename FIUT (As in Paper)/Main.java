import java.util.*;
import java.io.*;
public class Main{
    static BufferedReader br;
    static PrintWriter ot;
    public static void main(String args[]) throws IOException{
        try{
            br = new BufferedReader(new FileReader("input.txt"));
            ot = new PrintWriter(new FileWriter("output.txt"));

            D = new ArrayList<>();

            takeInput();
            // Uncomment the following line if Input is
            // in characters.
            // takeInputAsCharacters();

            double minimumSupport = 0.6;

            FIUT(D, minimumSupport);
            
            ot.close();
        } catch(Exception e){
            e.printStackTrace();
            return;
        }
    }
    static void FIUT(List<List<Integer>> D, double minimumSupport){
        List<List<List<Integer>>> hItemsets = kItemsetsGeneration(D, minimumSupport);
        print(hItemsets);
        // Uncomment the following line if Input is
        // in characters.
        // printCharacters(hItemsets);
        int minimumCountRequired = (int)(Math.ceil(minimumSupport * D.size()));
        for(int i = M; i >= 2; i--){
            FIUNode kFIUTree = kFIUTreeGeneration(hItemsets, i);
            List<List<Integer>> frequentItemSets = new ArrayList<>();
            frequentKItemSetsGenerator(kFIUTree, frequentItemSets, minimumCountRequired, new ArrayList<>());
            printFrequentItemSets(frequentItemSets, i);
            // Uncomment the following line if items is
            // in characters.
            // printFrequentItemSetsCharcaters(frequentItemSets, i);
        }
    }
    static void frequentKItemSetsGenerator(FIUNode kFIUTree,
                                        List<List<Integer>> frequentItemSets,
                                        int minimumCountRequired,
                                        List<Integer> temp){
        if(kFIUTree.isEnd){
            if(kFIUTree.freq >= minimumCountRequired){
                frequentItemSets.add(new ArrayList(temp));
            }
            return;
        }
        for(Map.Entry<Integer, FIUNode> e : kFIUTree.child.entrySet()){
            temp.add(e.getKey());
            frequentKItemSetsGenerator(e.getValue(), frequentItemSets, minimumCountRequired, temp);
            temp.remove(temp.size() - 1);
        }
    }
    static FIUNode kFIUTreeGeneration(List<List<List<Integer>>> hItemsets, int k){
        FIUNode root = new FIUNode();
        List<List<Integer>> temp = new ArrayList<>();
        for(List<Integer> items : hItemsets.get(k)){
            root.insertCustom(root, items);
        }
        for(int i = M; i > k; i--){
            for(List<Integer> items : hItemsets.get(i)){
                List<List<Integer>> subsets = new ArrayList<>();
                findKSizedSubsets(items, 0, subsets, new ArrayList<>(), k);
                for(List<Integer> subsetItem : subsets){
                    root.insertCustom(root, subsetItem);
                }
            }
        }

        return root;
    }
    static void printRootToLeavePath(FIUNode root, List<Integer> temp){
        if(root.isEnd){
            ot.println(temp+" "+root.freq);
        }
        for(Map.Entry<Integer, FIUNode> e : root.child.entrySet()){
            temp.add(e.getKey());
            printRootToLeavePath(e.getValue(), temp);
            temp.remove(temp.size()-1);
        }
    }
    static void findKSizedSubsets(List<Integer> items, int ind, List<List<Integer>> subsets, List<Integer> temp, int k){
        if(temp.size() == k){
            temp.add(items.get(items.size() - 1));
            subsets.add(new ArrayList(temp));
            temp.remove(temp.size() - 1);
            return;
        }
        if(ind == items.size() - 1)
            return;
        findKSizedSubsets(items, ind + 1, subsets, temp, k);
        temp.add(items.get(ind));
        findKSizedSubsets(items, ind + 1, subsets, temp, k);
        temp.remove(temp.size() - 1);
    }
    static List<List<List<Integer>>> kItemsetsGeneration(List<List<Integer>> D, 
                                                            double minimumSupport){
        Map<Integer, Integer> mapItemToFrequency = new HashMap<>();
        for(List<Integer> x : D)
            for(int y : x)
                mapItemToFrequency.put(y, mapItemToFrequency.getOrDefault(y, 0) + 1);

        M = 0;
        StorageNode root = new StorageNode();
        int minimumCountRequired = (int)(Math.ceil(minimumSupport * D.size()));
        List<List<List<Integer>>> hItemsets = new ArrayList<>();
        for(List<Integer> t : D){
            List<Integer> tPrime = new ArrayList<>();
            for(int item : t){
                if(mapItemToFrequency.get(item) >= minimumCountRequired){
                    tPrime.add(item);
                }
            }
            if(tPrime.size() > M)
                M = tPrime.size();

            while(hItemsets.size() <= M)
                hItemsets.add(new ArrayList<>());

            root.insert(root, tPrime);
        }

        kItemsetsGenerationHelper(root, hItemsets, 0, new ArrayList<>());
        return hItemsets;
    }
    static void kItemsetsGenerationHelper(StorageNode root, 
                                        List<List<List<Integer>>> hItemsets,
                                        int size,
                                        List<Integer> t){
        if(root.isEnd){
            t.add(root.freq);
            hItemsets.get(size).add(new ArrayList(t));
            t.remove(t.size() - 1);
        }
        for(Map.Entry<Integer, StorageNode> e : root.child.entrySet()){
            t.add(e.getKey());
            kItemsetsGenerationHelper(e.getValue(), hItemsets, size + 1, t);
            t.remove(t.size() - 1);
        }
    }
    static void printFrequentItemSets(List<List<Integer>> frequentItemSets, int size){
        ot.println(size+"-Frequent Item Sets are - ");
        for(int i = 0; i < frequentItemSets.size(); i++){
            ot.print(i+": ");
            ot.println(frequentItemSets.get(i));
        }
        ot.println("-----------------------------------");
    }
    static void print(List<List<List<Integer>>> list){
        for(int i = 1; i < list.size(); i++){
            ot.print(i+": ");
            ot.println(list.get(i));
        }
    }
    static void takeInput(){
        try{
            String S;
            int count = 0;
            while((S=br.readLine())!=null && count++ < 10000){
                String s[] = S.split(" ");
                List<Integer> temp = new ArrayList<>();
                for(int j=0;j<s.length;j++){
                    temp.add(Integer.parseInt(s[j]));
                }
                D.add(new ArrayList<>(temp));
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    static void takeInputAsCharacters(){
        try{
            String S;
            int count = 0;
            while((S=br.readLine())!=null && count++ < 10000){
                String s[] = S.split(" ");
                List<Integer> temp = new ArrayList<>();
                for(int j=0;j<s.length;j++){
                    temp.add((int)(s[j].charAt(0) - 'a' + 1));
                }
                D.add(new ArrayList<>(temp));
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    static void printFrequentItemSetsCharcaters(List<List<Integer>> frequentItemSets, int size){
        ot.println(size+"-Frequent Item Sets are - ");
        for(int i = 0; i < frequentItemSets.size(); i++){
            ot.print(i+": ");
            for(int c : frequentItemSets.get(i)){
                ot.print((char)(c + 'a' - 1) + " ");
            }
            ot.println();
        }
        ot.println("-----------------------------------");
    }
    static void printCharacters(List<List<List<Integer>>> list){
        for(int i = 1; i < list.size(); i++){
            ot.print(i+": ");
            for(List<Integer> l : list.get(i)){
                ot.print("[ ");
                for(int c : l){
                    ot.print((char)(c + 'a' - 1) + " ");
                }
                ot.print("] ");
            }
            ot.println();
        }
    }
    static int M;
    static List<List<Integer>> D;
}
class FIUNode{
    Map<Integer, FIUNode> child;
    int itemId;
    int freq;
    boolean isEnd;
    FIUNode(){
        freq = 0;
        isEnd = false;
        child = new HashMap<>();
    }
    FIUNode(int itemId){
        this.itemId = itemId;
        freq = 0;
        isEnd = false;
        child = new HashMap<>();
    }
    public void insert(FIUNode node, List<Integer> t){
        for(int ind = 0; ind < t.size(); ind++){
            if(!node.child.containsKey(t.get(ind)))
                node.child.put(t.get(ind), new FIUNode(t.get(ind)));
            node = node.child.get(t.get(ind));
            node.freq++;
        }
        node.isEnd = true;
    }
    public void insertCustom(FIUNode node, List<Integer> t){
        for(int ind = 0; ind < t.size() - 1; ind++){
            if(!node.child.containsKey(t.get(ind)))
                node.child.put(t.get(ind), new FIUNode(t.get(ind)));
            node = node.child.get(t.get(ind));
            node.freq += t.get(t.size() - 1);
        }
        node.isEnd = true;
    }
}
class StorageNode{
    Map<Integer, StorageNode> child;
    int itemId;
    int freq;
    boolean isEnd;
    StorageNode(){
        freq = 0;
        isEnd = false;
        child = new HashMap<>();
    };
    StorageNode(int itemId){
        this.itemId = itemId;
        freq = 0;
        isEnd = false;
        child = new HashMap<>();
    }
    public void insert(StorageNode node, List<Integer> t){
        for(int ind = 0; ind < t.size(); ind++){
            if(!node.child.containsKey(t.get(ind)))
                node.child.put(t.get(ind), new StorageNode(t.get(ind)));
            node = node.child.get(t.get(ind));
            node.freq++;
        }
        node.isEnd = true;
    }
    public void insertCustom(StorageNode node, List<Integer> t){
        for(int ind = 0; ind < t.size() - 1; ind++){
            if(!node.child.containsKey(t.get(ind)))
                node.child.put(t.get(ind), new StorageNode(t.get(ind)));
            node = node.child.get(t.get(ind));
            node.freq += t.get(t.size() - 1);
        }
        node.isEnd = true;
    }
}